﻿const versionRegex = /<Version>(\d+.\d+.\d+)<\/Version>/;

module.exports = {
    readVersion: function (contents) {
        return versionRegex.exec(contents)[1];
    },
    writeVersion: function (contents, version) {
        return contents.replace(versionRegex, `<Version>${version}</Version>`);
    }
}