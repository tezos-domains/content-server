# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.1](https://gitlab.com/tezos-domains/content-server/compare/v0.2.0...v0.2.1) (2022-10-13)


### Bug Fixes

* relative links in forwarded HTTP content should work better ([059c4ae](https://gitlab.com/tezos-domains/content-server/commit/059c4ae227c0dc5873139bbb1235084483e7006b))

## [0.2.0](https://gitlab.com/tezos-domains/content-server/compare/v0.1.0...v0.2.0) (2021-07-01)


### Features

* add http provider ([da38318](https://gitlab.com/tezos-domains/content-server/commit/da383182fb9cda3aacc26a057bc4abe7567f7e61))
* add redirect url support ([06adae6](https://gitlab.com/tezos-domains/content-server/commit/06adae6eaf5ba0a1a3f3f7e6f4bccabcfaf1bc73))


### Bug Fixes

* support cidv0 hashes ([96569bc](https://gitlab.com/tezos-domains/content-server/commit/96569bc315c30ba79a0a78ec6c5c06511ff4add2))

## 0.1.0 (2021-06-01)


### Features

* add health check endpoint for root page ([7c8b769](https://gitlab.com/tezos-domains/content-server/commit/7c8b7699551993a4a061e62b4e9fd4ee2a88e6bc))
* add ipfs node health check ([52d3f73](https://gitlab.com/tezos-domains/content-server/commit/52d3f730726277912a4853c36c3f373fd9033f3d))
* add ipns ([2b7ecaa](https://gitlab.com/tezos-domains/content-server/commit/2b7ecaaaaf94403be5b11dd6b77693577dd665f4)), closes [#3](https://gitlab.com/tezos-domains/content-server/issues/3)
* add prometheus metrics ([740b89f](https://gitlab.com/tezos-domains/content-server/commit/740b89f4072753365a42fd10c086e860a2c18514))
* add redirect to tezos domains from root ([980af00](https://gitlab.com/tezos-domains/content-server/commit/980af0033ef74b83e8562ccdcbbca238f48a51e1))
* add serilog logging ([7f929eb](https://gitlab.com/tezos-domains/content-server/commit/7f929eb5273b17d23e7c8cc49d8da9e1ba7570dc)), closes [#11](https://gitlab.com/tezos-domains/content-server/issues/11)
* add simple domain blacklist ([8731110](https://gitlab.com/tezos-domains/content-server/commit/873111092006d8b016f2d76949e968a9f7900570))
* propagate content headers ([eec2fb7](https://gitlab.com/tezos-domains/content-server/commit/eec2fb7746f42db71e2772ddc151b15f29412b2b))
* propagate headers to and from the gateway ([8c4a3d6](https://gitlab.com/tezos-domains/content-server/commit/8c4a3d678c93c2d57146b8b70c7e6b161865a24d))
* propagate status code from ipfs gateway ([8d97151](https://gitlab.com/tezos-domains/content-server/commit/8d971519603aeaa10fc8aaa7c7d34b7057aa8c9f)), closes [#7](https://gitlab.com/tezos-domains/content-server/issues/7)
