module.exports = {
    releaseCommitMessageFormat: 'release: cut the {{currentTag}} release',
    bumpFiles: [
        {
            filename: './src/TezosDomains.ContentServer.App/TezosDomains.ContentServer.App.csproj',
            updater: require('./csproj-updater.js')
        },
        {
            filename: './package.json',
            type: 'json'
        }
    ]
}
