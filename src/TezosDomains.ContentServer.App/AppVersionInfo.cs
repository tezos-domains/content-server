﻿using System.Linq;
using System.Reflection;

namespace TezosDomains.ContentServer.App
{
    public static class AppVersionInfo
    {
        public static readonly string Version;
        public static readonly string GitHash;
        public static readonly string ShortGitHash;

        static AppVersionInfo()
        {
            Version = "1.0.0+LOCALBUILD"; // Dummy version for local dev
            var appAssembly = typeof(AppVersionInfo).Assembly;
            var infoVerAttr = (AssemblyInformationalVersionAttribute?) appAssembly
                .GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute))
                .FirstOrDefault();

            if (infoVerAttr?.InformationalVersion.Length > 6)
            {
                // Hash is embedded in the version after a '+' symbol, e.g. 1.0.0+a34a913742f8845d3da5309b7b17242222d41a21
                Version = infoVerAttr.InformationalVersion;
            }

            GitHash = Version.Substring(Version.IndexOf('+') + 1);
            ShortGitHash = GitHash.Substring(0, 8);
        }
    }
}