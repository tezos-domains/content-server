﻿using System.ComponentModel.DataAnnotations;
using TezosDomains.ContentServer.App.Configuration;

namespace TezosDomains.ContentServer.App.Metrics
{
    public interface IMetricsConfiguration : IConfigurationInstance
    {
        [Required]
        bool IsEnabled { get; init; }
        
        [Required]
        string Endpoint { get; init; }
    }
    
    public class MetricsConfiguration : IMetricsConfiguration
    {
        public bool IsEnabled { get; init; }

        public string Endpoint { get; init; } = null!;

        object IConfigurationInstance.GetDiagnosticInfo() => new { IsEnabled };
    }
}