﻿using Microsoft.AspNetCore.Routing;
using Prometheus;

namespace TezosDomains.ContentServer.App.Metrics
{
    public static class EndpointRouteBuilderExtensions
    {
        public static void MapMetricsIfEnabled(
            this IEndpointRouteBuilder endpoints,
            IMetricsConfiguration configuration
        )
        {
            if (configuration.IsEnabled)
                endpoints.MapMetrics(configuration.Endpoint);
        }
    }

}