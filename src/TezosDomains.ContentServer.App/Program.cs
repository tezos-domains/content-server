using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using TezosDomains.ContentServer.App.Logging;

namespace TezosDomains.ContentServer.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LoggerInit.CreateLogger("TezosDomains.ContentServer");
            var logger = Log.ForContext<Program>();

            try
            {
                logger.Information("Starting up. Version: {version}", AppVersionInfo.Version);
                CreateHostBuilder(args).Build().Run();
                logger.Information("Shutting down");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Application start-up failed");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(
                    webBuilder => { webBuilder.UseStartup<Startup>(); }
                );
    }
}