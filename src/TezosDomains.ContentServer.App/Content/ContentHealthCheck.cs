﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public class ContentHealthCheck : IHealthCheck
    {
        private readonly IReadOnlyList<IContentProvider> _contentProviders;

        public ContentHealthCheck(IEnumerable<IContentProvider> contentProviders)
        {
            _contentProviders = contentProviders.ToList();
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken)
        {
            var healthy = await Task.WhenAll(_contentProviders.Select(c => c.IsHealthyAsync(cancellationToken)));
            
            if (healthy.All(h => h))
            {
                return HealthCheckResult.Healthy();
            }

            return HealthCheckResult.Unhealthy();
        }
    }
}