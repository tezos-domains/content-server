﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TezosDomains.ContentServer.App.Configuration;

namespace TezosDomains.ContentServer.App.Content
{
    public class AppRoot
    {
        [Required] public string Hostname { get; init; } = null!;

        [Required] public string Tld { get; init; } = null!;
    }

    public class ContentProvider
    {
        [Required] public Uri Url { get; init; } = null!;

        public Uri? ApiUrl { get; init; }
    }

    public interface IContentConfiguration : IConfigurationInstance
    {
        IReadOnlyList<AppRoot> AppRoots { get; }
        IReadOnlyDictionary<string, ContentProvider> Providers { get; }
        IReadOnlyList<string> BlacklistedDomains { get; }
        string? HostOverride { get; }
    }

    public class ContentConfiguration : IValidatableObject, IContentConfiguration
    {
        [Required, MinLength(1)] public IReadOnlyList<AppRoot> AppRoots { get; init; } = null!;

        [Required, MinLength(1)] public IReadOnlyDictionary<string, ContentProvider> Providers { get; init; } = null!;

        public IReadOnlyList<string> BlacklistedDomains { get; init; } = new List<string>();

        public string? HostOverride { get; init; }

        object IConfigurationInstance.GetDiagnosticInfo() => this;

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            foreach (var domainPattern in AppRoots)
            {
                foreach (var validationResult in domainPattern.GetValidationErrors(nameof(AppRoots)))
                {
                    yield return validationResult;
                }
            }
            
            foreach (var provider in Providers)
            {
                foreach (var validationResult in provider.Value.GetValidationErrors(nameof(AppRoots)))
                {
                    yield return validationResult;
                }
            }
        }
    }
}