﻿using StrawberryShake;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.ContentServer.ApiClient;

namespace TezosDomains.ContentServer.App.Content
{
    public class GqlDomainResolver : IDomainResolver
    {
        private readonly ITezosDomainsApi _api;

        public GqlDomainResolver(ITezosDomainsApi api)
        {
            _api = api;
        }
        
        public async Task<DomainWebInfo?> ResolveWebInfoAsync(string name, CancellationToken cancellationToken)
        {
            var result = await _api.GetDomainData.ExecuteAsync(name, cancellationToken);
            result.EnsureNoErrors();
            
            if (result.Data!.Domain == null)
            {
                return null;
            }

            ContentUri? contentUrl = null;
            Uri? redirectUrl = null;

            var webContentUrl = result.Data.Domain.Data.FirstOrDefault(d => d.Key == "web:content_url")?.Value;
            var webRedirectUrl = result.Data.Domain.Data.FirstOrDefault(d => d.Key == "web:redirect_url")?.Value;

            if (!string.IsNullOrWhiteSpace(webContentUrl))
            {
                ContentUri.TryParse(webContentUrl, out contentUrl);
            }

            if (!string.IsNullOrWhiteSpace(webRedirectUrl))
            {
                Uri.TryCreate(webRedirectUrl, UriKind.Absolute, out redirectUrl);
            }

            return new DomainWebInfo(contentUrl, redirectUrl);
        }
    }
}