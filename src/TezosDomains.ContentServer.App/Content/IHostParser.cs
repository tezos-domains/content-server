﻿namespace TezosDomains.ContentServer.App.Content
{
    public interface IHostParser
    {
        string? ParseDomain(string host);

        bool IsRoot(string host);
    }
}