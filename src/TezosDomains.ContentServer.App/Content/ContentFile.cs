﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net;

namespace TezosDomains.ContentServer.App.Content
{
    public record ContentFile (
        HttpStatusCode StatusCode,
        IHeaderDictionary Headers,
        Stream? Content
    );
}