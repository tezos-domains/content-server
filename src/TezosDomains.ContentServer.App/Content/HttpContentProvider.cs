﻿using Flurl;
using Microsoft.Extensions.Logging;
using System.Net.Http;

namespace TezosDomains.ContentServer.App.Content
{
    public class HttpContentProvider : HttpContentProviderBase
    {
        public HttpContentProvider(IHttpClientFactory httpClientFactory, ILoggerFactory loggerFactory)
            : base(httpClientFactory, loggerFactory.CreateLogger<HttpContentProvider>())
        {
        }

        public override bool CanProvide(ContentUri contentUrl)
        {
            return !contentUrl.IsIpfs;
        }

        protected override string BuildUrl(ContentUri contentUrl, string path)
        {
            var url = contentUrl.ToString();
            if (!url.EndsWith("/"))
            {
                url = url.RemovePathSegment();
            }
            return url.AppendPathSegment(path).ToString();
        }
    }
}