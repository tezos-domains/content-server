﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using TezosDomains.ContentServer.App.Configuration;

namespace TezosDomains.ContentServer.App.Content
{
    public static class ContentMiddlewareExtensions
    {
        public static IApplicationBuilder UseContent(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ContentMiddleware>();

            return builder;
        }


        public static IServiceCollection AddContent(this IServiceCollection services)
        {
            services.AddContentServerConfigurationWithDiagnostics<IContentConfiguration, ContentConfiguration>("Content");
            
            services.AddSingleton<IDomainResolver, GqlDomainResolver>();
            services.AddSingleton<IContentProvider, IpfsContentProvider>();
            services.AddSingleton<IContentProvider, HttpContentProvider>();
            services.AddSingleton<IHostResolver, HostResolver>();
            services.AddSingleton<IHostParser, HostParser>();
            services.AddSingleton<IBlacklist, StaticConfigBlacklist>();
            
            return services;
        }
    }
}