﻿using System.Linq;

namespace TezosDomains.ContentServer.App.Content
{
    public class HostParser : IHostParser
    {
        private readonly IContentConfiguration _contentConfiguration;

        public HostParser(IContentConfiguration contentConfiguration)
        {
            _contentConfiguration = contentConfiguration;
        }
        
        public string? ParseDomain(string host)
        {
            var appRoot = _contentConfiguration.AppRoots.FirstOrDefault(r => host.EndsWith(r.Hostname));

            if (appRoot == null || host == appRoot.Hostname)
            {
                return null;
            }

            return host.Remove(host.Length - appRoot.Hostname.Length) + appRoot.Tld;
        }

        public bool IsRoot(string host)
        {
            return _contentConfiguration.AppRoots.Any(r => host == r.Hostname);
        }
    }
}