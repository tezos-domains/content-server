﻿using Flurl;
using System;
using System.Text.RegularExpressions;

namespace TezosDomains.ContentServer.App.Content
{
    public class ContentUri
    {
        public string Scheme { get; }

        public string Host { get; }
        public int Port { get; }

        public string Path { get; }

        public bool IsIpfs { get; set; }

        public ContentUri(string url)
        {
            var uri = new Uri(url);
            Scheme = uri.Scheme;
            Path = uri.AbsolutePath;

            if (Scheme is "ipfs" or "ipns")
            {
                var m = Regex.Match(url, $"{Scheme}://([a-zA-Z0-9-]+)(?:{Path}|$)");
                Host = m.Groups[1].Value;
                IsIpfs = true;
            }
            else
            {
                Host = uri.Host;
                Port = uri.Port;
            }

            if (string.IsNullOrEmpty(Host))
            {
                throw new Exception($"Invalid URL {url}.");
            }
        }

        public override string ToString()
        {
            return $"{Scheme}://{Host}{(!IsIpfs ? $":{Port}" : string.Empty)}{Path}";
        }

        public static ContentUri Parse(string url)
        {
            return new ContentUri(url);
        }

        public static bool TryParse(string url, out ContentUri? contentUri)
        {
            try
            {
                contentUri = Parse(url);
                return true;
            }
            catch
            {
                contentUri = null;
                return false;
            }
        }
    }
}