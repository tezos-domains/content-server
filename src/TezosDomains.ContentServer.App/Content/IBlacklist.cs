﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public interface IBlacklist
    {
        Task<bool> IsBlacklistedAsync(string domain);

        Task AllowAsync(string domain, CancellationToken cancellationToken);
        
        Task BlacklistAsync(string domain, CancellationToken cancellationToken);
    }

    public class StaticConfigBlacklist : IBlacklist
    {
        private readonly IContentConfiguration _contentConfiguration;

        public StaticConfigBlacklist(IContentConfiguration contentConfiguration)
        {
            _contentConfiguration = contentConfiguration;
        }
        
        public Task<bool> IsBlacklistedAsync(string domain)
        {
            return Task.FromResult(_contentConfiguration.BlacklistedDomains.Any(d => d == domain));
        }

        public Task AllowAsync(string domain, CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }

        public Task BlacklistAsync(string domain, CancellationToken cancellationToken)
        {
            throw new NotSupportedException();
        }
    }
}