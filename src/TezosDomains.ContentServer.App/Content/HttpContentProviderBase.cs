﻿using Flurl;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.ContentServer.App.Http;

namespace TezosDomains.ContentServer.App.Content
{
    public abstract class HttpContentProviderBase : IContentProvider
    {
        protected readonly IHttpClientFactory HttpClientFactory;
        protected readonly ILogger Logger;

        private readonly string[] _headersToPass =
            { HeaderNames.ContentType, HeaderNames.ContentLength, HeaderNames.ETag, HeaderNames.LastModified, HeaderNames.CacheControl, "X-Ipfs-Path" };

        protected HttpContentProviderBase(IHttpClientFactory httpClientFactory, ILogger logger)
        {
            HttpClientFactory = httpClientFactory;
            Logger = logger;
        }

        public abstract bool CanProvide(ContentUri contentUrl);

        public async Task<ContentFile> GetAsync(ContentUri contentUrl, string path, IHeaderDictionary requestHeaders, CancellationToken cancellationToken)
        {
            var url = BuildUrl(contentUrl, path);

            using var client = HttpClientFactory.CreateClient();
            using var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Apply(requestHeaders);

            var response = await client.SendAsync(request, cancellationToken);

            var headers = response.Headers.Extract(_headersToPass);
            Stream? content = null;

            if (response.IsSuccessStatusCode)
            {
                headers.Apply(response.Content.Headers.Extract(_headersToPass));
                content = await response.Content.ReadAsStreamAsync(cancellationToken);
            }
            else if ((int) response.StatusCode >= 400 && response.StatusCode != HttpStatusCode.NotFound)
            {
                Logger.LogError("Request to {url} failed with status {status}.", url, response.StatusCode);
            }

            return new ContentFile(response.StatusCode, headers, content);
        }

        protected abstract string BuildUrl(ContentUri contentUrl, string path);

        public virtual Task<bool> IsHealthyAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }
    }
}