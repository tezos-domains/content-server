﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TezosDomains.ContentServer.App.Http;

namespace TezosDomains.ContentServer.App.Content
{
    public class ContentContext
    {
        private readonly HttpContext _context;
        private readonly IDomainResolver _domainResolver;
        private readonly IHostResolver _hostResolver;
        private readonly IHostParser _hostParser;
        private readonly IBlacklist _blacklist;
        private readonly IReadOnlyList<IContentProvider> _contentProviders;
        private readonly ILogger _logger;

        private readonly string[] _headerToPass = { HeaderNames.IfModifiedSince, HeaderNames.IfNoneMatch, HeaderNames.IfUnmodifiedSince, HeaderNames.IfMatch };

        public ContentContext(
            HttpContext context,
            IDomainResolver domainResolver,
            IHostResolver hostResolver,
            IHostParser hostParser,
            IBlacklist blacklist,
            IReadOnlyList<IContentProvider> contentProviders,
            ILogger logger
        )
        {
            _context = context;
            _domainResolver = domainResolver;
            _hostResolver = hostResolver;
            _hostParser = hostParser;
            _blacklist = blacklist;
            _contentProviders = contentProviders;
            _logger = logger;
        }

        public async Task ServeContentAsync()
        {
            try
            {
                var host = _hostResolver.Resolve(_context);
                var domain = _hostParser.ParseDomain(host) ?? throw new Exception($"Unable to parse domain from host {host}.");

                if (await _blacklist.IsBlacklistedAsync(domain))
                {
                    SetStatus(HttpStatusCode.Gone);
                    return;
                }
                
                var webInfo = await _domainResolver.ResolveWebInfoAsync(domain, _context.RequestAborted);
                if (webInfo == null)
                {
                    _logger.LogDebug("Web info for domain {domain} can't be resolved. Returning 404.", domain);
                    SetStatus(HttpStatusCode.NotFound);
                    return;
                }

                if (webInfo.RedirectUrl != null)
                {
                    _context.Response.Redirect(webInfo.RedirectUrl.AbsoluteUri);
                    return;
                }
                
                if (webInfo.ContentUrl == null)
                {
                    _logger.LogDebug("Content URL for domain {domain} can't be resolved. Returning 404.", domain);
                    SetStatus(HttpStatusCode.NotFound);
                    return;
                }

                var provider = _contentProviders.FirstOrDefault(p => p.CanProvide(webInfo.ContentUrl));
                if (provider == null)
                {
                    _logger.LogDebug("Content resolver for URL {url} is not registered. Returning 404.", webInfo);
                    SetStatus(HttpStatusCode.NotFound);
                    return;
                }

                var headers = _context.Request.Headers.Extract(_headerToPass);
                var content = await provider.GetAsync(webInfo.ContentUrl, _context.Request.Path, headers, _context.RequestAborted);

                SetStatus(content.StatusCode);
                SetHeaders(content.Headers);

                if (content.Content != null)
                {
                    try
                    {
                        await StreamCopyOperation.CopyToAsync(content.Content, _context.Response.Body, content.Content.Length, _context.RequestAborted);
                    }
                    finally
                    {
                        await content.Content.DisposeAsync();
                    }
                }
            }
            catch (TaskCanceledException e)
            {
                if (!e.CancellationToken.IsCancellationRequested)
                {
                    throw;
                }
            }

        }

        private void SetStatus(HttpStatusCode statusCode)
        {
            _context.Response.StatusCode = (int) statusCode;
        }

        private void SetHeaders(IHeaderDictionary headers)
        {
            _context.Response.Headers.Apply(headers);
        }
    }
}