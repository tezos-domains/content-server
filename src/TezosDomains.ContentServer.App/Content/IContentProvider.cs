﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public interface IContentProvider
    {
        bool CanProvide(ContentUri contentUrl);
        Task<ContentFile> GetAsync(ContentUri contentUrl, string path, IHeaderDictionary requestHeaders, CancellationToken cancellationToken);
        Task<bool> IsHealthyAsync(CancellationToken cancellationToken);
    }
}