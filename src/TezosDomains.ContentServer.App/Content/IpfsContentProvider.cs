﻿using Flurl;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public class IpfsContentProvider : HttpContentProviderBase
    {
        private readonly ContentProvider _config;

        public IpfsContentProvider(IHttpClientFactory httpClientFactory, IContentConfiguration contentConfiguration, ILoggerFactory loggerFactory)
            : base(httpClientFactory, loggerFactory.CreateLogger<IpfsContentProvider>())
        {
            _config = contentConfiguration.Providers["ipfs"];
        }

        public override bool CanProvide(ContentUri contentUrl)
        {
            return contentUrl.IsIpfs;
        }

        protected override string BuildUrl(ContentUri contentUrl, string path)
        {
            return _config.Url.AppendPathSegments(contentUrl.Scheme, contentUrl.Host, contentUrl.Path, path).ToString();
        }

        public override async Task<bool> IsHealthyAsync(CancellationToken cancellationToken)
        {
            if (_config.ApiUrl == null)
            {
                Logger.LogWarning($"{nameof(_config.ApiUrl)} not specified for provider {GetType().Name}. Assuming it's healthy.");
                return true;
            }

            var url = _config.ApiUrl.AppendPathSegment("/api/v0/swarm/peers");

            using var client = HttpClientFactory.CreateClient();

            var response = await client.PostAsync(url, new StringContent(string.Empty), cancellationToken);
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            var peers = await response.Content.ReadFromJsonAsync<PeersResponse>(cancellationToken: cancellationToken);
            if (peers == null || peers.Peers.Count == 0)
            {
                return false;
            }

            return true;
        }

        private class PeersResponse
        {
            public IReadOnlyList<Peer> Peers { get; init; } = null!;

            public class Peer
            {
                public string Addr { get; init; } = null!;
            }
        }
    }
}