﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System;

namespace TezosDomains.ContentServer.App.Content
{
    public class HostResolver : IHostResolver
    {
        private readonly IContentConfiguration _contentConfiguration;
        private readonly IHostEnvironment _hostEnvironment;

        public HostResolver(IContentConfiguration contentConfiguration, IHostEnvironment hostEnvironment)
        {
            _contentConfiguration = contentConfiguration;
            _hostEnvironment = hostEnvironment;
        }

        public string Resolve(HttpContext httpContext)
        {
            if (_hostEnvironment.IsDevelopment() && httpContext.Request.Query["root"] == "1")
            {
                return _contentConfiguration.AppRoots[0].Hostname;
            }
            
            if (!string.IsNullOrEmpty(_contentConfiguration.HostOverride))
            {
                if (!_hostEnvironment.IsDevelopment())
                {
                    throw new Exception($"{nameof(_contentConfiguration.HostOverride)} should not be set in non dev environments.");
                }

                return _contentConfiguration.HostOverride;
            }

            return httpContext.Request.Host.Host;
        }
    }
}