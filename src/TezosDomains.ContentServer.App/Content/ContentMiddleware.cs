﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public class ContentMiddleware
    {
        private readonly IDomainResolver _domainResolver;
        private readonly IHostResolver _hostResolver;
        private readonly IHostParser _hostParser;
        private readonly IBlacklist _blacklist;
        private readonly ILogger _logger;
        private readonly IReadOnlyList<IContentProvider> _contentResolvers;

        public ContentMiddleware(
            RequestDelegate next,
            IDomainResolver domainResolver,
            IHostResolver hostResolver,
            IHostParser hostParser,
            IBlacklist blacklist,
            IEnumerable<IContentProvider> contentResolvers,
            ILoggerFactory loggerFactory
        )
        {
            _domainResolver = domainResolver;
            _hostResolver = hostResolver;
            _hostParser = hostParser;
            _blacklist = blacklist;
            _logger = loggerFactory.CreateLogger<ContentMiddleware>();
            _contentResolvers = contentResolvers.ToList();
        }

        public async Task Invoke(HttpContext context)
        {
            var contentContext = new ContentContext(context, _domainResolver, _hostResolver, _hostParser, _blacklist, _contentResolvers, _logger);

            await contentContext.ServeContentAsync();
        }
    }
}