﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TezosDomains.ContentServer.App.Content
{
    public record DomainWebInfo(ContentUri? ContentUrl, Uri? RedirectUrl);
    
    public interface IDomainResolver
    {
        Task<DomainWebInfo?> ResolveWebInfoAsync(string name, CancellationToken cancellationToken);
    }
}