﻿using Microsoft.AspNetCore.Http;

namespace TezosDomains.ContentServer.App.Content
{
    public interface IHostResolver
    {
        string Resolve(HttpContext httpContext);
    }
}