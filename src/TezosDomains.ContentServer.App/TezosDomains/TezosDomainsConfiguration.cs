﻿using System;
using System.ComponentModel.DataAnnotations;
using TezosDomains.ContentServer.App.Configuration;

namespace TezosDomains.ContentServer.App.TezosDomains
{
    public interface ITezosDomainsConfiguration : IConfigurationInstance
    {
        Uri ApiUrl { get; init; }
        Uri AppUrl { get; init; }
        string? ApiClientId { get; init; }
    }
    
    public sealed class TezosDomainsConfiguration : ITezosDomainsConfiguration
    {
        [Required] public Uri ApiUrl { get; init; } = null!;

        [Required] public Uri AppUrl { get; init; } = null!;

        public string? ApiClientId { get; init; }

        object IConfigurationInstance.GetDiagnosticInfo() => this;
    }
}