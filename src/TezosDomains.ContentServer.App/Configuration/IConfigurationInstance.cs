﻿namespace TezosDomains.ContentServer.App.Configuration
{
    public interface IConfigurationInstance
    {
        string Name => GetType().Name;

        object GetDiagnosticInfo();
    }
}