﻿namespace TezosDomains.ContentServer.App.Configuration
{
    public sealed record ConfigurationWrapper(
        string Name,
        object DiagnosticInfo
    ) : IConfigurationInstance
    {
        string IConfigurationInstance.Name => Name;
        object IConfigurationInstance.GetDiagnosticInfo() => DiagnosticInfo;
    }
}