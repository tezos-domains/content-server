﻿using Microsoft.Extensions.Configuration;
using System;

namespace TezosDomains.ContentServer.App.Configuration
{
    public static class ConfigurationExtensions
    {
        public static TConfiguration GetValid<TConfiguration>(this IConfiguration configuration, string sectionName)
            where TConfiguration : class, new()
        {
            try
            {
                var section = configuration.GetSection(sectionName);
                var instance = section.Get<TConfiguration>() ?? new TConfiguration();

                instance.Validate();

                return instance;
            }
            catch (Exception ex)
            {
                throw new Exception($"Invalid configuration section '{sectionName}'.", ex);
            }
        }
    }
}