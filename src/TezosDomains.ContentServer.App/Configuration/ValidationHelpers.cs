﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TezosDomains.ContentServer.App.Configuration
{
    public static class ValidationHelpers
    {
        public static void Validate(this object obj)
            => Validator.ValidateObject(obj, new ValidationContext(obj), validateAllProperties: true);

        public static IEnumerable<ValidationResult> GetValidationErrors(this object obj)
        {
            var errors = new List<ValidationResult>();
            Validator.TryValidateObject(obj, new ValidationContext(obj), errors, validateAllProperties: true);
            return errors;
        }

        public static IEnumerable<ValidationResult> GetValidationErrors(this object obj, string propertyPath)
            => obj.GetValidationErrors()
                .Select(
                    error =>
                    {
                        var memberNames = error.MemberNames.ToList();
                        return new ValidationResult(
                            errorMessage: $"Invalid {propertyPath}: {error.ErrorMessage}",
                            memberNames: memberNames.Count > 0 ? memberNames.Select(n => $"{propertyPath}.{n}").ToList() : new[] { propertyPath }
                        );
                    }
                );
    }
}