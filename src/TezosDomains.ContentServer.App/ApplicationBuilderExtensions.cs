﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using TezosDomains.ContentServer.App.Content;
using TezosDomains.ContentServer.App.TezosDomains;

namespace TezosDomains.ContentServer.App
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder WhenRoot(this IApplicationBuilder builder, Action<IApplicationBuilder> configuration)
        {
            builder.MapWhen(IsRoot, configuration);
            
            return builder;
        }
        
        public static IApplicationBuilder WhenDomain(this IApplicationBuilder builder, Action<IApplicationBuilder> configuration)
        {
            builder.MapWhen(c => !IsRoot(c), configuration);
            
            return builder;
        }

        public static void UseRedirectToTezosDomains(this IApplicationBuilder builder)
        {
            builder.Run(
                c =>
                {
                    var config = c.RequestServices.GetRequiredService<ITezosDomainsConfiguration>();
                    c.Response.Redirect(config.AppUrl.AbsoluteUri);
                    
                    return Task.CompletedTask;
                });
        } 
        
        private static bool IsRoot(HttpContext c)
        {
            var hostResolver = c.RequestServices.GetRequiredService<IHostResolver>();
            var hostParser = c.RequestServices.GetRequiredService<IHostParser>();

            var host = hostResolver.Resolve(c);

            return hostParser.IsRoot(host);
        }
    }
}