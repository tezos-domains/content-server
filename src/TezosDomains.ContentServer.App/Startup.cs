using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Prometheus;
using Serilog;
using TezosDomains.ContentServer.App.Configuration;
using TezosDomains.ContentServer.App.Content;
using TezosDomains.ContentServer.App.Metrics;
using TezosDomains.ContentServer.App.TezosDomains;

namespace TezosDomains.ContentServer.App
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddContentServerConfigurationWithDiagnostics<ITezosDomainsConfiguration, TezosDomainsConfiguration>("TezosDomains");
            services.AddContentServerConfigurationWithDiagnostics<IMetricsConfiguration, MetricsConfiguration>("Metrics");

            services.AddHealthChecks().AddCheck<ContentHealthCheck>("Content").ForwardToPrometheus();

            services.AddHttpClient(Options.DefaultName).UseHttpClientMetrics();

            services
                .AddTezosDomainsApi()
                .ConfigureHttpClient(
                    (provider, client) =>
                    {
                        var config = provider.GetRequiredService<ITezosDomainsConfiguration>();
                        client.BaseAddress = config.ApiUrl;
                        client.DefaultRequestHeaders.Add("X-ClientId", config.ApiClientId);
                    },
                    builder => builder.UseHttpClientMetrics()
                );
            services.AddContent();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMetricsConfiguration metricsConfiguration)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();
            app.UseStatusCodePages();

            app.WhenRoot(
                a => a.UseRouting()
                    .UseEndpoints(
                        endpoints =>
                        {
                            endpoints.MapMetricsIfEnabled(metricsConfiguration);
                            endpoints.MapHealthChecks("/health");
                        }
                    )
                    .UseRedirectToTezosDomains()
            );

            app.WhenDomain(
                a => app.UseHttpMetrics()
                    .UseContent()
            );
        }
    }
}