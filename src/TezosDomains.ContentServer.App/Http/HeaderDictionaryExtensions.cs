﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace TezosDomains.ContentServer.App.Http
{
    public static class HeaderDictionaryExtensions
    {
        public static void Apply(this IHeaderDictionary to, IHeaderDictionary from)
        {
            foreach (var (key, value) in from)
            {
                to.Remove(key);
                to.Add(key, value);
            }
        }
        
        public static void Apply(this HttpRequestHeaders to, IHeaderDictionary from)
        {
            foreach (var (key, value) in from)
            {
                to.Remove(key);
                to.Add(key, value.ToArray());
            }
        }

        private static IHeaderDictionary ExtractCore<TDictionary>(
            TDictionary headers,
            Func<TDictionary, string, StringValues?> valueGetter,
            IEnumerable<string> headerNames
        )
        {
            var result = new HeaderDictionary();

            foreach (var header in headerNames)
            {
                var value = valueGetter(headers, header);
                if (value.HasValue)
                {
                    result.Add(header, value.Value);
                }
            }

            return result;
        }

        public static IHeaderDictionary Extract(this HttpResponseHeaders from, IEnumerable<string> headerNames)
        {
            return ExtractCore(
                from,
                (headers, name) => headers.TryGetValues(name, out var value) ? new StringValues(value.ToArray()) : (StringValues?) null,
                headerNames
            );
        }
        
        public static IHeaderDictionary Extract(this HttpContentHeaders from, IEnumerable<string> headerNames)
        {
            return ExtractCore(
                from,
                (headers, name) => headers.TryGetValues(name, out var value) ? new StringValues(value.ToArray()) : (StringValues?) null,
                headerNames
            );
        }

        public static IHeaderDictionary Extract(this IHeaderDictionary from, IEnumerable<string> headerNames)
        {
            return ExtractCore(
                from,
                (headers, name) => headers.TryGetValue(name, out var value) ? value : (StringValues?) null,
                headerNames
            );
        }
    }
}