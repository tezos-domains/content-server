﻿using FluentAssertions;
using TezosDomains.ContentServer.App.Content;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class ContentUriTests
    {
        [InlineData("ipfs://xxx", "ipfs", "xxx", "/")]
        [InlineData("ipfs://XxX", "ipfs", "XxX", "/")]
        [InlineData("ipfs://xxx/some/path", "ipfs", "xxx", "/some/path")]
        [Theory]
        public void ContentUri_ShouldParseUrlCorrectly(string url, string expectedScheme, string expectedHost, string expectedPath)
        {
            var uri = new ContentUri(url);

            uri.Scheme.Should().Be(expectedScheme);
            uri.Host.Should().Be(expectedHost);
            uri.Path.Should().Be(expectedPath);
        }

        [Fact]
        public void ContentUrl_TryParse_ShouldParseUrl()
        {
            ContentUri.TryParse("ipfs://xxx", out var uri).Should().BeTrue();

            uri?.ToString().Should().Be("ipfs://xxx/");
        }
        
        [InlineData("%@!&#^*@!")]
        [InlineData("haHAA")]
        [InlineData("ipfs://aa#aa")]
        [InlineData("ipfs/aaaa")]
        [Theory]
        public void ContentUrl_TryParse_ShouldReturnFalseWhenInvalid(string url)
        {
            ContentUri.TryParse(url, out var uri).Should().BeFalse();

            uri.Should().BeNull();
        }
    }
}