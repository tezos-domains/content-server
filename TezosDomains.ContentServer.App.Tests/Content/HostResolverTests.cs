using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;
using NSubstitute;
using System;
using System.Collections.Generic;
using TezosDomains.ContentServer.App.Content;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class HostResolverTests
    {
        private readonly IHostResolver _resolver;
        private readonly HttpContext _httpContextMock;
        private readonly IContentConfiguration _contentConfigurationMock;
        private readonly IHostEnvironment _hostEnvironmentMock;

        public HostResolverTests()
        {
            _httpContextMock = Substitute.For<HttpContext>();
            _hostEnvironmentMock = Substitute.For<IHostEnvironment>();
            _contentConfigurationMock = Substitute.For<IContentConfiguration>();

            _httpContextMock.Request.Host.Returns(new HostString("alice.tez.page"));
            
            _contentConfigurationMock.AppRoots.Returns(
                new[]
                {
                    new AppRoot { Hostname = "tez.page", Tld = "tez"}
                }
            );

            _resolver = new HostResolver(_contentConfigurationMock, _hostEnvironmentMock);
        }

        [Fact]
        public void Resolve_ShouldReturnHostFromHttpContext()
        {
            var host = _resolver.Resolve(_httpContextMock);

            host.Should().Be("alice.tez.page");
        }
        
        [Fact]
        public void Resolve_ShouldResolveHostFromOverride()
        {
            _contentConfigurationMock.HostOverride.Returns("bob.tez.page");
            _hostEnvironmentMock.EnvironmentName.Returns(Environments.Development);
            
            var host = _resolver.Resolve(_httpContextMock);

            host.Should().Be("bob.tez.page");
        }
        
        [Fact]
        public void Resolve_ShouldResolveRootFromQsWhenInDev()
        {
            _contentConfigurationMock.HostOverride.Returns("bob.tez.page");
            _hostEnvironmentMock.EnvironmentName.Returns(Environments.Development);
            _httpContextMock.Request.Query.Returns(new QueryCollection(new Dictionary<string, StringValues> { { "root", "1" } }));
            
            var host = _resolver.Resolve(_httpContextMock);

            host.Should().Be("tez.page");
        }
        
        [Fact]
        public void Resolve_ShouldNotResolveRootFromQsWhenNotInDev()
        {
            _hostEnvironmentMock.EnvironmentName.Returns(Environments.Production);
            _httpContextMock.Request.Query.Returns(new QueryCollection(new Dictionary<string, StringValues> { { "root", "1" } }));
            
            var host = _resolver.Resolve(_httpContextMock);

            host.Should().Be("alice.tez.page");
        }
        
        [Fact]
        public void Resolve_ShouldThrowIfHostOverrideIsSetOutsideDevelopment()
        {
            _contentConfigurationMock.HostOverride.Returns("bob.tez.page");
            _hostEnvironmentMock.EnvironmentName.Returns(Environments.Production);
            
            Action action = () => _resolver.Resolve(_httpContextMock);

            action.Should().Throw<Exception>().WithMessage("HostOverride should not be set in non dev environments.");
        }
    }
}