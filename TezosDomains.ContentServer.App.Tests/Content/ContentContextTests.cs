﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.ContentServer.App.Content;
using TezosDomains.ContentServer.App.Tests.Utils;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class ContentContextTests
    {
        private readonly ContentContext _context;
        private readonly HttpContext _httpContextMock;
        private readonly IDomainResolver _domainResolverMock;
        private readonly IHostParser _hostParserMock;
        private readonly IHostResolver _hostResolverMock;
        private readonly IBlacklist _blacklistMock;
        private readonly IContentProvider _contentProviderMock;
        private readonly CancellationToken _ct;
        private readonly HeaderDictionary _requestHeaders;
        private readonly HeaderDictionary _responseHeaders;
        private readonly HttpResponse _httpResponseMock;

        public ContentContextTests()
        {
            _httpContextMock = Substitute.For<HttpContext>();
            _domainResolverMock = Substitute.For<IDomainResolver>();
            _hostParserMock = Substitute.For<IHostParser>();
            _hostResolverMock = Substitute.For<IHostResolver>();
            _blacklistMock = Substitute.For<IBlacklist>();
            _contentProviderMock = Substitute.For<IContentProvider>();
            _httpResponseMock = Substitute.For<HttpResponse>();
            _ct = TestCancellationToken.Get();
            _requestHeaders = new HeaderDictionary();
            _responseHeaders = new HeaderDictionary();

            _httpContextMock.RequestAborted.Returns(_ct);
            _httpContextMock.Request.Headers.Returns(_requestHeaders);
            _httpContextMock.Request.Path.Returns(new PathString("/path"));
            _httpContextMock.Response.Returns(_httpResponseMock);
            _httpResponseMock.Headers.Returns(_responseHeaders);
            _httpResponseMock.Body.Returns(new MemoryStream());

            _hostResolverMock.Resolve(_httpContextMock).Returns("alice.dev.page");
            _hostParserMock.ParseDomain("alice.dev.page").Returns("alice.tez");
            _domainResolverMock.ResolveWebInfoAsync("alice.tez", _ct)
                .Returns(Task.FromResult<DomainWebInfo?>(new DomainWebInfo(new ContentUri("ipfs://hash"), null)));

            _contentProviderMock.CanProvide(default!).ReturnsForAnyArgs(true);

            _contentProviderMock.GetAsync(Arg.Is<ContentUri>(x => x.ToString() == "ipfs://hash/"), "/path", Arg.Any<HeaderDictionary>(), _ct)
                .Returns(
                    Task.FromResult(new ContentFile(HttpStatusCode.OK, new HeaderDictionary(), new MemoryStream(Encoding.UTF8.GetBytes("pepega"))))
                );

            _context = new ContentContext(
                _httpContextMock,
                _domainResolverMock,
                _hostResolverMock,
                _hostParserMock,
                _blacklistMock,
                new[] { _contentProviderMock },
                new NullLogger<ContentContext>()
            );
        }

        [Fact]
        public async Task ShouldServeContent()
        {
            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(200);
            StreamUtil.Read(_httpResponseMock.Body).Should().Be("pepega");
        }

        [Fact]
        public async Task ShouldPassSomeHeadersFromRequestToProvider()
        {
            _requestHeaders.Add(HeaderNames.IfMatch, "etag");
            _requestHeaders.Add(HeaderNames.IfNoneMatch, "etag2");
            _requestHeaders.Add(HeaderNames.IfModifiedSince, "Wed, 21 Oct 2015 07:28:00 GMT");
            _requestHeaders.Add(HeaderNames.IfUnmodifiedSince, "Wed, 21 Oct 2014 07:28:00 GMT");
            _requestHeaders.Add(HeaderNames.Cookie, "ha=lol");

            await _context.ServeContentAsync();

            await _contentProviderMock.Received()
                .GetAsync(
                    Arg.Any<ContentUri>(),
                    Arg.Any<string>(),
                    Arg.Is<HeaderDictionary>(
                        d =>
                            d.Count == 4
                            && d.Contains(new KeyValuePair<string, StringValues>(HeaderNames.IfMatch, "etag"))
                            && d.Contains(new KeyValuePair<string, StringValues>(HeaderNames.IfNoneMatch, "etag2"))
                            && d.Contains(new KeyValuePair<string, StringValues>(HeaderNames.IfModifiedSince, "Wed, 21 Oct 2015 07:28:00 GMT"))
                            && d.Contains(new KeyValuePair<string, StringValues>(HeaderNames.IfUnmodifiedSince, "Wed, 21 Oct 2014 07:28:00 GMT"))
                    ),
                    _ct
                );

            _httpResponseMock.StatusCode.Should().Be(200);
            StreamUtil.Read(_httpResponseMock.Body).Should().Be("pepega");
        }

        [Fact]
        public async Task ShouldAddHeadersFromProviderToTheResponse()
        {
            _contentProviderMock.GetAsync(Arg.Is<ContentUri>(x => x.ToString() == "ipfs://hash/"), "/path", Arg.Any<HeaderDictionary>(), _ct)
                .Returns(
                    Task.FromResult(
                        new ContentFile(
                            HttpStatusCode.OK,
                            new HeaderDictionary { { HeaderNames.ContentType, "text/plain" } },
                            new MemoryStream(Encoding.UTF8.GetBytes("pepega"))
                        )
                    )
                );

            _responseHeaders.Add(HeaderNames.CacheControl, "no-cache");

            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(200);
            StreamUtil.Read(_httpResponseMock.Body).Should().Be("pepega");

            _responseHeaders.Count.Should().Be(2);

            _responseHeaders.Should()
                .Contain(
                    new KeyValuePair<string, StringValues>(HeaderNames.CacheControl, "no-cache"),
                    new KeyValuePair<string, StringValues>(HeaderNames.ContentType, "text/plain")
                );
        }
        
        [Fact]
        public async Task ShouldReturnGoneForBlacklistedDomains()
        {
            _blacklistMock.IsBlacklistedAsync("alice.tez").Returns(Task.FromResult(true));
            
            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(410);
            _httpResponseMock.Body.Length.Should().Be(0);
        }
        
        [Fact]
        public async Task ShouldReturnNotFoundForDomainsThatDontExist()
        {
            _domainResolverMock.ResolveWebInfoAsync("alice.tez", _ct).Returns((DomainWebInfo?)null);
            
            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(404);
            _httpResponseMock.Body.Length.Should().Be(0);
        }
        
        [Fact]
        public async Task ShouldReturnRedirectIfRedirectUrlIsConfigured()
        {
            _domainResolverMock.ResolveWebInfoAsync("alice.tez", _ct)
                .Returns(Task.FromResult<DomainWebInfo?>(new DomainWebInfo(new ContentUri("ipfs://hash"), new Uri("https://redirect.com"))));
            
            await _context.ServeContentAsync();

            _httpResponseMock.Received().Redirect("https://redirect.com/");
        }
        
        [Fact]
        public async Task ShouldReturnNotFoundIfWebsitesDataIsNotConfigured()
        {
            _domainResolverMock.ResolveWebInfoAsync("alice.tez", _ct)
                .Returns(Task.FromResult<DomainWebInfo?>(new DomainWebInfo(null, null)));
            
            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(404);
            _httpResponseMock.Body.Length.Should().Be(0);
        }
        
        [Fact]
        public async Task ShouldReturnNotFoundIfThereIsNoProviderThatCanProvide()
        {
            _contentProviderMock.CanProvide(default!).ReturnsForAnyArgs(false);
            
            await _context.ServeContentAsync();

            _httpResponseMock.StatusCode.Should().Be(404);
            _httpResponseMock.Body.Length.Should().Be(0);
        }
    }
}