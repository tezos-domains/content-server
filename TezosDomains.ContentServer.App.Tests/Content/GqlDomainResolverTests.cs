﻿using FluentAssertions;
using NSubstitute;
using StrawberryShake;
using System;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.ContentServer.ApiClient;
using TezosDomains.ContentServer.App.Content;
using TezosDomains.ContentServer.App.Tests.Utils;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class GqlDomainResolverTests
    {
        private readonly IDomainResolver _domainResolver;
        private readonly IOperationResult<IGetDomainDataResult> _apiResult;
        private readonly IGetDomainDataResult _apiResultData;
        private readonly CancellationToken _ct;

        public GqlDomainResolverTests()
        {
            _ct = TestCancellationToken.Get();
            var tezosDomainsApiMock = Substitute.For<ITezosDomainsApi>();
            _apiResult = Substitute.For<IOperationResult<IGetDomainDataResult>>();
            _apiResultData = Substitute.For<IGetDomainDataResult>();

            _apiResult.Data.Returns(_apiResultData);

            tezosDomainsApiMock.GetDomainData.ExecuteAsync("alice.tez", _ct).Returns(_apiResult);

            _domainResolver = new GqlDomainResolver(tezosDomainsApiMock);
        }

        [Fact]
        public async Task ResolveWebInfoAsync_ShouldReturnWebInfo()
        {
            _apiResultData.Domain.Returns(new GetDomainData_Domain_Domain(new[]
            {
                new GetDomainData_Domain_Data_DataItem("web:content_url", "ipfs://xxx"),
                new GetDomainData_Domain_Data_DataItem("web:redirect_url", "https://lol.com")
            }));

            var webInfo = await _domainResolver.ResolveWebInfoAsync("alice.tez", _ct);

            webInfo?.ContentUrl?.ToString().Should().Be("ipfs://xxx/");
            webInfo?.RedirectUrl.Should().Be("https://lol.com");
        }
        
        [Fact]
        public async Task ResolveWebInfoAsync_ShouldReturnNullIfDomainDoesNotExist()
        {
            _apiResultData.Domain.Returns((IGetDomainData_Domain?) null);
            
            var webInfo = await _domainResolver.ResolveWebInfoAsync("alice.tez", _ct);

            webInfo.Should().BeNull();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task ResolveWebInfoAsync_ShouldReturnNullIfUrlIsEmpty(string? dataValue)
        {
            _apiResultData.Domain.Returns(new GetDomainData_Domain_Domain(new[]
            {
                new GetDomainData_Domain_Data_DataItem("web:content_url", dataValue),
                new GetDomainData_Domain_Data_DataItem("web:redirect_url", dataValue)
            }));

            var webInfo = await _domainResolver.ResolveWebInfoAsync("alice.tez", _ct);

            webInfo?.ContentUrl.Should().BeNull();
            webInfo?.RedirectUrl.Should().BeNull();
        }

        [Fact]
        public async Task ResolveWebInfoAsync_ShouldReturnNullIfWebContentUrlIsNotSpecified()
        {
            _apiResultData.Domain.Returns(new GetDomainData_Domain_Domain(new[] { new GetDomainData_Domain_Data_DataItem("openid:name", "Alice") }));

            var webInfo = await _domainResolver.ResolveWebInfoAsync("alice.tez", _ct);

            webInfo?.ContentUrl.Should().BeNull();
            webInfo?.RedirectUrl.Should().BeNull();
        }


        [Fact]
        public async Task ResolveWebContentUrl_ShouldThrowIfThereAreGqlErrors()
        {
            _apiResult.Errors.Returns(new[] { new ClientError("err") });

            Func<Task> action = async () => await _domainResolver.ResolveWebInfoAsync("alice.tez", _ct);

            await action.Should().ThrowAsync<Exception>();
        }
    }
}