﻿using FluentAssertions;
using NSubstitute;
using TezosDomains.ContentServer.App.Content;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class HostParserTests
    {
        private readonly IHostParser _hostParser;
        private readonly IContentConfiguration _contentConfigurationMock;

        public HostParserTests()
        {
            _contentConfigurationMock = Substitute.For<IContentConfiguration>();

            _contentConfigurationMock.AppRoots.Returns(
                new[]
                {
                    new AppRoot { Hostname = "tez.page", Tld = "tez"},
                    new AppRoot { Hostname = "tez.to", Tld = "ha" }
                }
            );

            _hostParser = new HostParser(_contentConfigurationMock);
        }

        [Theory]
        [InlineData("alice.tez.page", "alice.tez")]
        [InlineData("sub.alice.tez.page", "sub.alice.tez")]
        [InlineData("alice.tez.to", "alice.ha")]
        [InlineData("tez.to", null)]
        [InlineData("tez.page", null)]
        public void ParseDomain_ShouldParseDomainFromHostname(string host, string? expectedDomain)
        {
            var domain = _hostParser.ParseDomain(host);
            domain.Should().Be(expectedDomain);
        }

        [Theory]
        [InlineData("tez.page", true)]
        [InlineData("tez.to", true)]
        [InlineData("alice.tez.xxx", false)]
        [InlineData("alice.tez", false)]
        [InlineData("wut.page", false)]
        public void IsRoot_ShouldReturnWhetherTheHostIsOneOfAppRoots(string host, bool expectedIsRoot)
        {
            var isRoot = _hostParser.IsRoot(host);

            isRoot.Should().Be(expectedIsRoot);
        }
    }
}