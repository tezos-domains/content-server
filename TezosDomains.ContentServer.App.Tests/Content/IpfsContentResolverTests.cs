﻿using Castle.Core.Logging;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Net.Http.Headers;
using NSubstitute;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TezosDomains.ContentServer.App.Content;
using TezosDomains.ContentServer.App.Tests.Utils;
using Xunit;

namespace TezosDomains.ContentServer.App.Tests.Content
{
    public class IpfsContentResolverTests
    {
        private const string IpfsUrl = "https://ipfs.com";
        private readonly CancellationToken _ct;
        private readonly IContentProvider _contentProvider;
        private readonly MockHttpMessageHandler _httpMessageHandlerMock;
        private readonly IHeaderDictionary _headers;

        public IpfsContentResolverTests()
        {
            _ct = TestCancellationToken.Get();
            var httpClientFactoryMock = Substitute.For<IHttpClientFactory>();
            var contentConfigurationMock = Substitute.For<IContentConfiguration>();
            _httpMessageHandlerMock = new MockHttpMessageHandler();
            _headers = new HeaderDictionary();

            var httpClient = _httpMessageHandlerMock.ToHttpClient();

            httpClientFactoryMock.CreateClient().Returns(httpClient);

            contentConfigurationMock.Providers.Returns(
                new Dictionary<string, ContentProvider>
                {
                    { "ipfs", new ContentProvider { Url = new Uri(IpfsUrl) } }
                }
            );

            _contentProvider = new IpfsContentProvider(httpClientFactoryMock, contentConfigurationMock, new NullLoggerFactory());
        }

        [Fact]
        public async Task GetAsync_ShouldReturnContentFile()
        {
            _headers.Add(HeaderNames.IfNoneMatch, @"""etag""");
            _httpMessageHandlerMock.When($"{IpfsUrl}/ipfs/xxx/aaa.png")
                .With(r => r.Method == HttpMethod.Get && r.Headers.IfNoneMatch.ToString() == @"""etag""")
                .Respond(req =>
                    {
                        var response =  new HttpResponseMessage();
                        response.Content = new StringContent("pepega", null, MediaTypeNames.Text.Html);
                        response.Headers.ETag = new System.Net.Http.Headers.EntityTagHeaderValue(@"""newetag""");
                        response.StatusCode = HttpStatusCode.OK;

                        return response;
                    }
                );

            var file = await _contentProvider.GetAsync(new ContentUri("ipfs://xxx"), "/aaa.png", _headers, _ct);

            file.StatusCode.Should().Be(HttpStatusCode.OK);
            file.Headers[HeaderNames.ETag].Should().BeEquivalentTo(@"""newetag""");
            file.Headers[HeaderNames.ContentType].Should().BeEquivalentTo($"{MediaTypeNames.Text.Html}; charset=utf-8");
            StreamUtil.Read(file.Content).Should().Be("pepega");
        }

        [Fact]
        public async Task GetAsync_ShouldSupportIpns()
        {
            _httpMessageHandlerMock.When($"{IpfsUrl}/ipns/xxx/aaa.png").With(r => r.Method == HttpMethod.Get).Respond(new StringContent("pepega"));

            var file = await _contentProvider.GetAsync(new ContentUri("ipns://xxx"), "/aaa.png", _headers, _ct);

            file.StatusCode.Should().Be(HttpStatusCode.OK);
            StreamUtil.Read(file.Content).Should().Be("pepega");
        }
        
        [Fact]
        public async Task GetAsync_ShouldSupportPath()
        {
            _httpMessageHandlerMock.When($"{IpfsUrl}/ipns/xxx/bbb/aaa.png").With(r => r.Method == HttpMethod.Get).Respond(new StringContent("pepega"));

            var file = await _contentProvider.GetAsync(new ContentUri("ipns://xxx/bbb"), "/aaa.png", _headers, _ct);

            file.StatusCode.Should().Be(HttpStatusCode.OK);
            StreamUtil.Read(file.Content).Should().Be("pepega");
        }

        [Fact]
        public async Task GetAsync_ShouldReturnRootContentFile()
        {
            _httpMessageHandlerMock.When($"{IpfsUrl}/ipfs/xxx").With(r => r.Method == HttpMethod.Get).Respond(new StringContent("pepega"));

            var file = await _contentProvider.GetAsync(new ContentUri("ipfs://xxx"), "", _headers, _ct);

            file.StatusCode.Should().Be(HttpStatusCode.OK);
            StreamUtil.Read(file.Content).Should().Be("pepega");
        }

        [Fact]
        public async Task GetAsync_ShouldReturnPassThroughFailedResponse()
        {
            _httpMessageHandlerMock.When($"{IpfsUrl}/ipfs/xxx/aaa.png").With(r => r.Method == HttpMethod.Get).Respond(HttpStatusCode.InternalServerError);

            var file = await _contentProvider.GetAsync(new ContentUri("ipfs://xxx"), "/aaa.png", _headers, _ct);

            file.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
            file.Content.Should().BeNull();
        }

        [Theory]
        [InlineData("ipfs", true)]
        [InlineData("ipns", true)]
        [InlineData("http", false)]
        public void CanProvide_ShouldReturnTrueForIpfsAndIpns(string scheme, bool expected)
        {
            var uri = new ContentUri($"{scheme}://something");

            _contentProvider.CanProvide(uri).Should().Be(expected);
        }
    }
}