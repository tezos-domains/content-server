﻿using System.IO;

namespace TezosDomains.ContentServer.App.Tests.Utils
{
    public static class StreamUtil
    {
        public static string? Read(Stream? stream)
        {
            if (stream == null)
            {
                return null;
            }

            stream.Seek(0, SeekOrigin.Begin);

            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}