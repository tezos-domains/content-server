﻿using System.Threading;

namespace TezosDomains.ContentServer.App.Tests.Utils
{
    public static class TestCancellationToken
    {
        /// <summary>Create a fake using CTS in order to be unique each time.</summary>
        public static CancellationToken Get()
            => new CancellationTokenSource().Token;
    }
}