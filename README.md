# Tezos Domains Content Server

Serves content as configured with the `web:content_url` and `web:redirect_url` data fields on a Tezos Domain.

## Local setup

1. Put a test host name (e.g. `bruh.dev.page`) into an `HostOverride` property of `ContentConfiguration`
